package driveitServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientListener implements Runnable{
    private ServerSocket serverSocket;

    @Override
    public void run() {
        System.out.println("ClientListener is running..");
        
        try {
            serverSocket = new ServerSocket(Server.port);
            while(true) {
            	System.out.println("ClientListener: Waiting for clients");
                Socket clientSocket = serverSocket.accept(); //Accepting client
                Thread thread = new Thread( new ClientThread(clientSocket)); //creates thread
                thread.start(); //Run Client Thread                
            }
        }
        catch(IOException e) {
            e.printStackTrace(System.err);
        }
    }
}
