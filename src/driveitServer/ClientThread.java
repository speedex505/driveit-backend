package driveitServer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public final class ClientThread implements Runnable{
    Socket socket;
    PrintStream out;
    BufferedInputStream in;
    StringBuilder msg;
    String message;
    private int timeout = 5000;
    
	ClientThread(Socket s) {
	    msg= new StringBuilder();
	    this.socket = s;
	    
	    try {
	    	s.setSoTimeout(timeout);
	        out = new PrintStream(socket.getOutputStream()); //vytvorit PrintStream
	        in = new BufferedInputStream( socket.getInputStream());
	    }
	    catch(IOException e){
	    	System.err.println("Creating error");
	        e.printStackTrace(System.err);
	        close();
	    }
	}

    public String recieveMessage() throws IOException{
        StringBuilder b;
		b = new StringBuilder(Load(in));        
        b.replace(b.length()-2, b.length(), "");
        if("err".equals(b.toString())){return String.valueOf(Code.CLOSE.getCode());}
        return b.toString();        
    }
	void close() {
	    try {
	    	System.out.println("ClientThread: Client disconnected");
	        out.close();
	        in.close();
	        socket.close();
	        //Thread.currentThread().interrupt();
	    } catch(IOException e) {
	    	System.err.println("Closing error");
	    	e.printStackTrace(System.err);
	    }
	}

	private static String Load(BufferedInputStream in) throws IOException{
	    int c=0;
	    StringBuilder message= new StringBuilder();
	    while (true) {
	        c=in.read();
	        if(c==-1){return "error";}
	        message.append( (char)c ) ;
	        if((char)c=='\n'&&message.charAt(message.length()-2)=='\r'){
	            break;
	        }
	    }
	    return message.toString();
	}


	@Override
	public void run() {
		String message = String.valueOf(Code.LISTENING.getCode());
		while(true){
			out.print(message+"\r\n");
			out.flush();
			System.out.println("ClientThread: Message("+message+") sent to ("+socket.getInetAddress()+")");
			try {
				message=recieveMessage();
				System.out.println("ClientThread: Message("+message+") recieved");
			} catch (IOException e) {
				break;
			}
			if(Integer.parseInt(message.split(" ")[0])==Code.CLOSE.getCode()){
				break;
			}
			message = processIt(message);
			if(message==String.valueOf(Code.CLOSE.getCode())){
				break;
			}
		}
		close();
	}

	private String processIt(String input) {
		String [] message =input.split(" ");
		int code = Integer.parseInt(message[0]);
		if(code==Code.REGISTER.getCode()){
			System.out.println("ClientThread: Registration: "+message[1]+" "+message[2]);
			return registerUser(message);
		}
		if(code==Code.LOGIN.getCode()){
			System.out.println("ClientThread: Login: "+message[1]+" "+message[2]);
			return loginUser(message);
		}
		System.out.println("ClientThread: Bad command: "+input);
		return String.valueOf(Code.CLOSE.getCode());
	}

	private String loginUser(String[] input) {
		Database db = new Database();
		db.open();
		String result= db.work(input);
		db.close();
		return result;
	}
	private String registerUser(String[] input) {
		Database db = new Database();
		db.open();
		String result = db.work(input);
		db.close();
		return result;				
	}
}
