package driveitServer;
	
import java.io.IOException;

public class Server {
	    public static final int port=9876;

	    public static void main(String[] args) throws IOException {
	        System.out.println("Starting server...");
	        ClientListener clientListener = new ClientListener();
	        clientListener.run();
	    }
}
